Interactive Demo
===
Clicky draggy interactive demo of the thing explained below is [here](http://jcreed.org/js/fair-weighting/a.html).

* Click and drag on points to move them around.
* Shift-click and drag to duplicate.
* Control-click to delete.

Fair Weighting
===========

Suppose I want to have a collection of people vote on a place to put a landfill. I let each one vote for a point in the plane, and take the mean of all the locations. Problematically, some of them are able to stuff the ballot while I'm not looking, and replicate their vote N times, for a large N, and effectively outvote everyone else. I try to account for this by removing duplicate points. They respond by duplicating their votes but also moving them a tiny bit away from their favored location.

Can I do anything to reduce the impact of this cheating?

Is there a way of transforming a set V of points in R^n ("the votes") to a single point u(V) in R^n ("the outcome") such that

* It is continuous almost everywhere on its domain
* It commutes with rotations, translations, and uniform scaling (i.e. if you scale the x-coordinate by R, you must also scale all other coordinates by R)
* It yields the same answer if any vote is duplicated and displaced by an infinitesimal amount

?

Yes, there is:

Suppose the points V are V1, ... Vn. Compute the n x n matrix D such that Dij is the euclidean distance from Vi to Vj. Let I be the n x 1 vector whose entries are all 1.
Then w = D^(-1) I is an n x 1 vector which we interpret as (non-normalized) weights on all the votes. Normalize w by scaling it until it sums to 1. The outcome u(V) is then the sum of wi * Vi  for i from 1 to n

This is easily seen to commute with affine transformations since it constructs an affine combination of the existing points that only depends on their distances, and scaling up all the distances by a constant factor is obviated by the normalization of the weights at the end anyhow.

The reason that it is invulnerable to trivial vote duplication is as follows. What the computation D^(-1) I is really about is finding a weighting of all votes such that the weighted average distance from Vi to all the points in V is constant across all i. If this condition is met for some weights on the points in V, then it is also met if we duplicate a point and divide its weight in some way between its two mitosis-children. Assuming D is nonsingular, there will be exactly one way to do this, and the total weight attached to the duplicated votes must therefore be the same as the original vote.

Also apparently I had implemented this in ocaml [long ago](http://jcreed.livejournal.com/1245260.html) and used to think it had [something to do with inflation](http://jcreed.livejournal.com/2008/10/04/).