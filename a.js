$(go);
var RAD = 10;

function go() {
  fullscreen();
  var state = [{x:-300,y:-300},{x:100,y:0},{y:200,x:0}];
  $("#c").on('mousedown', function(e) {
    var p = vminus(relpos(e, c), {x: w/2, y: h/2});
    for(var i = 0; i < state.length; i++) {
      if (distance(p, state[i]) < RAD) {
	var j = i;
	if (e.shiftKey) {
	  state.push({x:state[i].x, y:state[i].y});
	  j = state.length - 1;
	}
	if (e.ctrlKey) {
	  if (state.length >= 3) {
	    state.splice(i, 1);
	    render(state);
	  }
	  return;
	}
	var orig_c = {x: state[i].x, y: state[i].y};
	var orig_m = {x: p.x, y: p.y};
	$(document).on('mousemove.drag', function(e) {
	  var p = vminus(relpos(e, c), {x: w/2, y: h/2});
	  state[j].x = p.x - orig_m.x + orig_c.x;
	  state[j].y = p.y - orig_m.y + orig_c.y;
	  render(state);
	});
	$(document).on('mouseup.drag', function(e) {
	  $(document).off('.drag');
	});
	break;
      }
    }
  });
  render(state);
}

function render(state) {
  d.fillStyle = "#888";
  d.fillRect(0,0,w,h);
  d.save();
  d.translate(w/2, h/2);

  var v = derive_weights(state);
  var center = v.c;
  var weights = v.w;

  _.each(state, function(p, ix) {
    d.beginPath();
    d.arc(p.x, p.y, 4 * RAD * Math.abs(weights[ix]), 0, 2 * Math.PI);
    d.fillStyle = weights[ix] > 0 ? "#338" : "#833";
    d.fill();
    d.beginPath();
    d.arc(p.x, p.y, RAD, 0, 2 * Math.PI);
    d.strokeStyle = "#eee";
    d.lineWidth = 1.01;
    d.stroke();

  });
  d.beginPath();
  d.arc(center.x, center.y, RAD, 0, 2 * Math.PI);
  d.fillStyle = "#383";
  d.fill();

  d.restore();
}

function manhattan(a, b) {
  return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
}
var metric = distance;

function derive_weights(state) {
  var mat = _.map(state, function(a) {
    return _.map(state, function(b) {
      return metric(a, b);
    });
  });
  var vec = _.map(state, function(a) { return 1; });

  var weight;
  try {
    if (Math.abs(numeric.det(mat)) < 0.00001) {
      throw "nope";
    }
    weight = numeric.dot(numeric.inv(mat), vec);
  }
  catch (e) {
    return {c: {x: 0, y: 0}, w: _.map(state, function(a) { return 0; })};
  }
  var normalized = numeric.dot(weight, 1 / numeric.sum(weight));
  var center = {x:0, y:0};
  _.each(state, function(p, ix) {
    center.x += normalized[ix] * p.x;
    center.y += normalized[ix] * p.y;
  });
  return {c: center, w: normalized};
}
